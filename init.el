;; =============================================================================
;; Initialize
(require 'package)
(setq package-archives '(("gnu"   . "http://elpa.zilongshanren.com/gnu/")
                         ("melpa" . "http://elpa.zilongshanren.com/melpa/")))
(package-initialize)

(unless (package-installed-p 'use-package)
  (progn
    (package-refresh-contents)
    (package-install 'use-package)))
(require 'use-package)

(setq use-package-always-defer t
      use-package-always-ensure t)
;;basic
(setq-default indent-tabs-mode nil)
(setq-default line-spacing 5)
(prefer-coding-system 'utf-8)
(setq buffer-file-coding-system 'utf-8-unix)
(setq default-file-name-coding-system 'utf-8-unix)
(setq default-keyboard-coding-system 'utf-8-unix)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))
(setq default-sendmail-coding-system 'utf-8-unix)
(setq default-terminal-coding-system 'utf-8-unix)
(setq inhibit-startup-screen t)
(setq frame-title-format "emacs@%b")
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed 0.02)
(setq mouse-wheel-follow-mouse 't)


(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
(when window-system
  (scroll-bar-mode -1))
(global-hl-line-mode t)
;;(global-linum-mode t)

;;theme
;; (use-package color-theme-solarized
;;   :init
;;   (progn
;;     (setq solarized-bold t)
;;     (setq solarized-italic nil)
;;     (setq solarized-visibility 'high)
;;     (set-frame-parameter nil 'background-mode 'dark)
;;     (set-terminal-parameter nil 'background-mode 'dark)
;;     (load-theme 'solarized t)))
(use-package spacemacs-theme		
  :ensure t
  :init (load-theme 'spacemacs-light t))
;;M-x
(use-package smex
  :ensure t
  :bind (("M-x" . smex)))

;;ido
(defun ido-define-keys ()
  (bind-key "C-n" 'ido-next-match ido-completion-map)
  (bind-key "C-p" 'ido-prev-match ido-completion-map))
(use-package ido
  :ensure t
  :init (progn
          (ido-mode t)
          (add-hook 'ido-setup-hook 'ido-define-keys)
          (add-to-list 'ido-ignore-buffers
                       "*nrepl-messages .+*")))

;; rainbow-delimiters
(use-package rainbow-delimiters
  :ensure t
  :init (progn
          (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)))
;;yasnippet
(use-package auto-yasnippet                  
  :ensure t
  :init (progn
          (yas-global-mode t)))

;;displays the key bindings following your currently entered incomplete command (a prefix) in a popup
(use-package which-key
  :ensure t
  :init (progn
          (which-key-mode)
          (which-key-setup-side-window-bottom)))
;;evil
(use-package evil-leader
  :ensure t)
(use-package evil
  :ensure t
  :init (progn
          (global-evil-leader-mode t)
          (evil-mode t)))
;;auto-complete
;; (use-package auto-complete
;;   :ensure t
;;   :init (progn
;;             (ac-config-default)))
;;company-mode
(use-package company
  :ensure t)
;;avy
(use-package avy
  :ensure t)
;;web-mode
(use-package web-mode
  :ensure t
  :init (progn
          (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))))
(server-start)

;;tabbar
(use-package tabbar
  :ensure t
  :init (progn
          (tabbar-mode)
          ))
;;my-func
(defun recent-buffer ()
  "Switch to other buffer"
  (interactive)
  (switch-to-buffer (other-buffer)))
(defun open-init-el ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
(defun open-file-for-default-app ()
  (interactive)
  (shell-command (concat "open " (buffer-name))))
;;key bind

(evil-leader/set-leader "<SPC>")
(evil-leader/set-key
  ;;basic
  "o" 'other-window
  "s" 'save-buffer
  "<tab>" 'recent-buffer
  "<SPC>" 'avy-goto-word-or-subword-1
  ;;buffer
  "bs" 'save-some-buffers
  "bb" 'switch-to-buffer
  "bk" 'kill-buffer-and-window
  "bn" 'next-buffer
  "bp" 'previous-buffer
  ;;window
  "wr" 'split-window-right
  "wb" 'split-window-below
  "wk" 'delete-window
  "wo" 'delete-other-windows
  ;;file
  "ff" 'ido-find-file
  "fd" 'open-init-el
  "fo" 'open-file-for-default-app
  ;;yasnippet
  "yc" 'aya-create
  "ye" 'aya-expand
  "yn" 'yas-new-snippet
  "yl" 'yas-load-snippet-buffer
  )
;;web-mode key bind
(evil-leader/set-key-for-mode 'web-mode
  "t" 'web-mode-fold-or-unfold
  "%" 'web-mode-tag-match
  ",i" 'web-mode-element-insert
  ",m" 'web-mode-mark-and-expand
  ",x" 'web-mode-dom-xpath
  ",e" 'web-mode-dom-errors-show
  ",f" 'web-mode-dom-normalize
  ",c" 'web-mode-element-clone
  ",t" 'web-mode-element-children-fold-or-unfold
  ",d" 'web-mode-element-kill
  ",n" 'web-mode-element-next
  ",p" 'web-mode-element-previous
  ",w" 'web-mode-element-wrap
  
  )

(bind-key "C-j" 'scroll-up-command)
(bind-key "C-k" 'scroll-down-command)
